from setuptools import setup

package_name = 'basic_robot'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='joselo',
    maintainer_email='josekun13@gmail.com',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'arduino_test = basic_robot.arduino_test:main','image_publisher = basic_robot.vision_aquire:main','image_subscriber = basic_robot.vision_reciver:main','interface_arduino = basic_robot.interface_arduino:main'
        ],
    },
)
