import rclpy
from rclpy.node import Node
from std_msgs.msg import String
import serial

class arduino_interface(Node):

    def __init__(self):
        super().__init__("interface_arduino")
        self.sensors_pub=self.create_publisher(String,"/sensors",10)
        self.motors_sub = self.create_subscription(String,'/testtop',self.velocities_callback, 10)
        self.motors_sub
        timer_period=0.1
        self.ser1=serial.Serial("/dev/ttyUSB0",57600,timeout=0)
        self.ser2=serial.Serial("/dev/ttyUSB1",9600,timeout=0)
        self.timer=self.create_timer(timer_period,self.timer_callback)

    def timer_callback(self):
        msg=String()
        s=self.ser1.readline()
        msg.data=str(s)
        self.sensors_pub.publish(msg)

    def velocities_callback(self,msg: String):
        vel=str.encode(msg.data)
        self.ser2.write(vel)
        

def main(args=None):
    rclpy.init(args=args)
    testcomnode=arduino_interface()
    rclpy.spin(testcomnode)
    testcomnode.destroy_node()
    rclpy.shutdown()

if __name__=='__main__':
    main()
