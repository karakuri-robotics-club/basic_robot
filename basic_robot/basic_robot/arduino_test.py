#!/usr/bin/env python3
import rclpy
from rclpy.node import Node
from std_msgs.msg import String
import serial


class arduino_test(Node):

    def __init__(self):
        super().__init__("arduino_test")
        self.chatter_pub=self.create_publisher(String,"/chatter",10)
        timer_period=1
        self.ser=serial.Serial("/dev/ttyACM0",9600,timeout=0)
        self.timer=self.create_timer(timer_period,self.timer_callback)

    def timer_callback(self):
        msg=String()
        self.ser.write(b"hola desde ROS2")
        s=self.ser.readline()
        msg.data= str(s)
        self.chatter_pub.publish(msg)
        self.get_logger().info("publishing hola desde ROS2")

        

def main(args=None):
    rclpy.init(args=args)
    testcomnode=arduino_test()
    rclpy.spin(testcomnode)
    testcomnode.destroy_node()
    rclpy.shutdown()

if __name__=='__main__':
    main()
