/*Club de robotica Karakuri
 * Name:Ein Interface ROS Motors
 * Version: 1.0
 * Date: 06-06-2023
 * Description:This code recive the velocities
 * for the motors
 * Mantainer: Jose Angel Martinez Navarro
 * E-Mail: josekun13@ieee.org
 * License: X11
 */
#include <AX12A.h>

#define DirectionPin   (10u)
#define BaudRate      (9600ul)
#define IDL  (1u)
#define IDR  (2u)


String Input;
int Lvel=0;
int Rvel=0;

void setup(){ 
  ax12a.begin(BaudRate, DirectionPin, &Serial);
  ax12a.setEndless(IDL, ON);
  ax12a.setEndless(IDR, ON);
  ax12a.turn(IDL, LEFT, 0);
  ax12a.turn(IDR, LEFT, 0);
  Serial.begin(9600);
}

void loop() {
  while (Serial.available() == 0) {}  
  Input = Serial.readString();
  Input.trim();
  int index=Input.indexOf(',');
  int length2 = Input.length();
  String Left = Input.substring(0,index);
  String Rigth = Input.substring(index+1, length2);
  Lvel=Left.toInt();
  Rvel=Rigth.toInt();
  //backward left
  if(Lvel<0)
  {
    Lvel=-Lvel;
    if (Lvel>1000){
      Lvel=1000;
    }
    ax12a.turn(IDL,RIGHT, Lvel);
  }
  //forward left
  else{
    if (Lvel>1000){
      Lvel=1000;
    }
    ax12a.turn(IDL,LEFT, Lvel);
  }

  //backward RIGHT
  if(Rvel<0)
  {
    Rvel=-Rvel;
    if (Rvel>1000){
      Rvel=1000;
    }
    ax12a.turn(IDR,LEFT, Rvel);
  }
  //forward RIGHT
  else{
    if (Rvel>1000){
      Rvel=1000;
    }
    ax12a.turn(IDR,RIGHT, Rvel);
  }
  delay(10);

}
