/*Club de robotica Karakuri
 * Name:Ein Interface ROS Sensors
 * Version: 1.0
 * Date: 06-06-2023
 * Description:This scipt read all robot's sensors
 * and sen the information using serial
 * Mantainer: Jose Angel Martinez Navarro
 * E-Mail: josekun13@ieee.org
 * License: X11
 */
#include <NewPing.h>
#include <Wire.h>




#define SONAR_NUM   2 // Number or sensors.
#define MAX_DISTANCE 200 // Max distance in cm.
#define PING_INTERVAL 33 // Milliseconds between pings.


const int PIRPin= 9;
const int MPU_addr=0x68; // I2C address of the MPU-6050
float AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ; //Imu Variables
unsigned long pingTimer[SONAR_NUM]; // When each pings.
unsigned int cm[SONAR_NUM]; // Store ping distances.
uint8_t currentSensor = 0; // Which sensor is active.
unsigned int usL;  //Distance to transmit left sensor
unsigned int usR;  //Distance to transmit right sensor
String returnString; //Sensor return
int32_t x=0;
int pirValue=0;


NewPing sonar[SONAR_NUM] = { // Sensor object array.
  NewPing(4, 5, MAX_DISTANCE),
  NewPing(6, 7, MAX_DISTANCE)
};
 
void setup() {
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B); // PWR_MGMT_1 register
  Wire.write(0); // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  pinMode(PIRPin, INPUT);
  Serial.begin(57600);
   
   pingTimer[0] = millis() + 75; // First ping start in ms.
   for (uint8_t i = 1; i < SONAR_NUM; i++)
    pingTimer[i] = pingTimer[i - 1] + PING_INTERVAL;
   
}

void loop() {
   readUS();
   readIMU();
   readPir();
   returnString=String(usL);
   returnString=returnString+" ";
   returnString=returnString+String(usR);
   returnString=returnString+" ";
   returnString=returnString+String(AcX/16384);
   returnString=returnString+" ";
   returnString=returnString+String(AcY/16384);
   returnString=returnString+" ";
   returnString=returnString+String(AcZ/16384);
   returnString=returnString+" ";
   returnString=returnString+String(Tmp);
   returnString=returnString+" ";
   returnString=returnString+String(GyX/16384);
   returnString=returnString+" ";
   returnString=returnString+String(GyY/16384);
   returnString=returnString+" ";
   returnString=returnString+String(GyZ/16384);
   returnString=returnString+" ";
   returnString=returnString+String(pirValue);
   returnString=returnString+" ";
   returnString=returnString+String(millis()-x);
   
   x=millis();
   Serial.println(returnString);
   delay(10);
   
   
}


void readIMU(){
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B); // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true); // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read(); // 0x3B (ACCEL_XOUT_H) &amp; 0x3C (ACCEL_XOUT_L)
  AcY=Wire.read()<<8|Wire.read(); // 0x3D (ACCEL_YOUT_H) &amp; 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read(); // 0x3F (ACCEL_ZOUT_H) &amp; 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read(); // 0x41 (TEMP_OUT_H) &amp; 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read(); // 0x43 (GYRO_XOUT_H) &amp; 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read(); // 0x45 (GYRO_YOUT_H) &amp; 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read(); // 0x47 (GYRO_ZOUT_H) &amp; 0x48 (GYRO_ZOUT_L)
  Tmp=(Tmp-1600)/340+36.56;
}

void readUS(){
  for (uint8_t i = 0; i < SONAR_NUM; i++) {
    if (millis() >= pingTimer[i]) {
      pingTimer[i] += PING_INTERVAL * SONAR_NUM;
      if (i == 0 && currentSensor == SONAR_NUM - 1)
        oneSensorCycle(); // Do something with results.
      sonar[currentSensor].timer_stop();
      currentSensor = i;
      cm[currentSensor] = 0;
      sonar[currentSensor].ping_timer(echoCheck);
    }
  }
}

void echoCheck() { // If ping echo, set distance to array.
  if (sonar[currentSensor].check_timer())
    cm[currentSensor] = sonar[currentSensor].ping_result / US_ROUNDTRIP_CM;
}

void readPir(){
  pirValue=digitalRead(PIRPin);
}
 
void oneSensorCycle() { // Do something with the results.
  usL=cm[0];
  usR=cm[1];
}
